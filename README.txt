SamPaste v1.1

Installation
	- Upload sampaste files to your server
	- Update config with your information (line 19-29 in config.php)
	  - If you changed DB_PREFIX please update the sampaste.sql on lines 8 & 9
	- Import sampaste.sql into your database
	- 'hlight' and 'raw' directories need to be writeable