<?php /*====================================================================================
		SamPaste [http://samjlevy.com/sampaste], open-source code sharing application
    	sam j levy [http://samjlevy.com]

    	This program is free software: you can redistribute it and/or modify it under the
    	terms of the GNU General Public License as published by the Free Software
    	Foundation, either version 3 of the License, or (at your option) any later
    	version.

    	This program is distributed in the hope that it will be useful, but WITHOUT ANY
    	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    	PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    	You should have received a copy of the GNU General Public License along with this
    	program.  If not, see <http://www.gnu.org/licenses/>.
      ====================================================================================*/

// Change these
define('DB_HOST','localhost'); // database host
define('DB_USER','user'); // database user
define('DB_PASS','password'); // database password
define('DB_NAME','sampaste'); // database name
define('DB_PREFIX','sampaste_'); // database table prefix
define('SITE_NAME','SamPaste');
define('SITE_PATH','http://sitepath'); // public URL to SamPaste installation- WITHOUT trailing slash
define('HR_OFFSET',0); // hour difference from server time, ex: +1,-1
define('PRUNE_DAYS',10); // number of days before a post is pruned
define('MAX_LENGTH',500000); // maximum number of characters that can be pasted
define('CLEAR_PASSWORD','password'); // visiting "http://[YOUR SITE]/clear/[PASSWORD]" - forces delete of all data in the app

// Don't need to change these
define('HLIGHT_PATH','hlight/');
define('RAW_PATH','raw/');
define('VERSION','1.1');
define('GESHI_VERSION','1.0.8.10');

// start a connection to the database
$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die ('Error connecting to database.');

// check if we can select the table
mysql_select_db(DB_NAME) or die ("Could not select database.");
?>