<?php /*====================================================================================
		SamPaste [http://samjlevy.com/sampaste], open-source code sharing application
    	sam j levy [http://samjlevy.com]

    	This program is free software: you can redistribute it and/or modify it under the
    	terms of the GNU General Public License as published by the Free Software
    	Foundation, either version 3 of the License, or (at your option) any later
    	version.

    	This program is distributed in the hope that it will be useful, but WITHOUT ANY
    	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    	PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    	You should have received a copy of the GNU General Public License along with this
    	program.  If not, see <http://www.gnu.org/licenses/>.
      ====================================================================================*/ ?>

<br />

<div class="footer_bar"><a href="http://samjlevy.com/sampaste" target="_blank"><strong>SamPaste</strong></a> <?php echo VERSION; ?>, <a href="http://samjlevy.com" target="_blank">sam j levy</a></div>
<br />

</body>
</html>