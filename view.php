<?php /*====================================================================================
		SamPaste [http://samjlevy.com/sampaste], open-source code sharing application
    	sam j levy [http://samjlevy.com]

    	This program is free software: you can redistribute it and/or modify it under the
    	terms of the GNU General Public License as published by the Free Software
    	Foundation, either version 3 of the License, or (at your option) any later
    	version.

    	This program is distributed in the hope that it will be useful, but WITHOUT ANY
    	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    	PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    	You should have received a copy of the GNU General Public License along with this
    	program.  If not, see <http://www.gnu.org/licenses/>.
      ====================================================================================*/
	  
include("head.php");

// query selection
$q_paste = "SELECT *, date_format(date_sub(date, INTERVAL " . HR_OFFSET . " HOUR), '%a. %b %e, %Y %h:%i %p') AS PasteDate FROM " . DB_PREFIX . "pastes WHERE id = '" . mysql_real_escape_string(trim($_REQUEST["id"])) . "'";

// assign query action to a variable, upon failure.. die
$q_paste_result = mysql_query($q_paste) or die("Querying database failed.");

// count the number of rows found
$q_paste_count = mysql_num_rows($q_paste_result);

if($q_paste_count != 0) {
	
	// read selection
	while ($q_paste_row = mysql_fetch_array($q_paste_result)) {
		// use db_id instead of id to avoid ambiguity with _REQUEST["id"]
		$db_id 		= $q_paste_row["id"];
		$date		= $q_paste_row["PasteDate"];
		$language	= trim($q_paste_row["language"]);
		$len		= number_format(trim($q_paste_row["len"]));
		$line		= number_format(trim($q_paste_row["line"]));
		$title 		= trim($q_paste_row["title"]);
		$creator	= trim($q_paste_row["creator"]);
		$visibility	= $q_paste_row["visibility"];
	}
?>

	<table width="100%">
      <tr>
        <td width="10px">&nbsp;</td>
        <td><font class="paste_id"><?php echo htmlentities($_REQUEST["id"]); ?></font><br />
        <font class="paste_details">Language: <?php echo $language; ?>
        <br />Length: <?php echo $len; ?> (<?php echo $line; ?> lines)
		<?php if($title != "") { ?><br />Title: <?php echo $title; } ?>
		<?php if($creator != "") { ?><br />Creator: <?php echo $creator; } ?>
        <br />Date: <?php echo $date; ?>
        <br />Visibility: <?php if($visibility == 1){ echo "Public"; } else { echo "Private"; } ?>
        </font><br /><br />
		<a href="<?php echo SITE_PATH; ?>">index</a> / <?php if(!isset($_REQUEST["raw"])) { echo "<a href='/v/" . $db_id . "/r'>view raw</a>"; } else { echo "<a href='/v/" . $db_id . "'>view highlighted</a>"; } ?></td>
      </tr></table>

<table width="100%">
<tr><td width="10px">&nbsp;</td><td>
<?php if($_REQUEST["raw"] == 1) {

	$raw_text_file = RAW_PATH . $db_id . ".txt";
	$raw_text_file_open = fopen($raw_text_file,"r");
	$raw_text = fread($raw_text_file_open,filesize($raw_text_file));
	fclose($raw_text_file_open); ?>

    <br />
    
	<script language="Javascript">
	function selectAll(theField) {
	  var tempval=eval("document."+theField)
	  tempval.focus()
	  tempval.select()
	}
	</script>
    
    <form name="form_raw">
    <input onclick="javascript:selectAll('form_raw.raw');" type="button" value="Select All"><br />
    <textarea name="raw" rows="20" style="width:98%;"><?php echo htmlspecialchars($raw_text); ?></textarea></form>

<?php } else { include( HLIGHT_PATH . $db_id . ".html"); } ?>
</td></tr></table>

<?php } else { ?>

<center><br /><br /><br /><span class="msg_confirm">Invalid ID.</span><br /><br />
<a href="./">index</a><br /><br /><br />

<?php }

include("foot.php"); ?>