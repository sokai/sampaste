<?php /*====================================================================================
		SamPaste [http://samjlevy.com/sampaste], open-source code sharing application
    	sam j levy [http://samjlevy.com]

    	This program is free software: you can redistribute it and/or modify it under the
    	terms of the GNU General Public License as published by the Free Software
    	Foundation, either version 3 of the License, or (at your option) any later
    	version.

    	This program is distributed in the hope that it will be useful, but WITHOUT ANY
    	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    	PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    	You should have received a copy of the GNU General Public License along with this
    	program.  If not, see <http://www.gnu.org/licenses/>.
      ====================================================================================*/
	  
include("head.php"); ?>

<br />

<?php

// query pastes table in database
$q_list_pastes = "SELECT *, date_format(date_sub(date, INTERVAL " . HR_OFFSET . " HOUR), '%a. %b %e, %Y %h:%i %p') AS PasteDate FROM " . DB_PREFIX . "pastes WHERE visibility = 1 ORDER BY date DESC";

// assign query action to a variable, upon failure.. die
$q_list_pastes_result = mysql_query($q_list_pastes) or die("Querying database failed.");

// count the number of rows found
$q_list_pastes_count = mysql_num_rows($q_list_pastes_result);

?>

<center><a href="/p">paste new</a><br /><br />

<table width="80%" align="center" cellpadding="5" cellspacing="0" class="paste_list">
	<tr class="table_head"><td width="20%">Paste ID</td><td width="5%">Lang</td><td width="10%">Length</td><td width="20%">Title</td><td width="20%">Creator</td><td width="25%">Date</td></tr>

    <?php // if a row is found, pastes exist in the database
    if($q_list_pastes_count !== 0) {
        while ($q_list_pastes_row = mysql_fetch_array($q_list_pastes_result)) {
            $count++;

            $id 	 	= $q_list_pastes_row["id"];
			$date		= $q_list_pastes_row["PasteDate"];
            $language	= trim($q_list_pastes_row["language"]);
			$len		= number_format(trim($q_list_pastes_row["len"]));
			$line		= number_format(trim($q_list_pastes_row["line"]));
            $title 		= trim($q_list_pastes_row["title"]);
            $creator	= trim($q_list_pastes_row["creator"]);

			echo "<tr" .  ( ($count & 1) ? " class='row_alt'" : "" ) . "><td><a href='/v/" . $id . "'>" . $id . "</a></td><td>" . $language . "</td><td>" . $len . " (" . $line . " lines)</td><td>" . $title . "</td><td>" . $creator . "</td><td>" . $date . "</td></tr>";
		}
	}
	else { echo "<tr class='row_alt'><td colspan='6'>None</td></tr>"; }
	?>
    
</table>
<br />
<?php include("foot.php"); ?>