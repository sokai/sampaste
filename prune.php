<?php /*====================================================================================
		SamPaste [http://samjlevy.com/sampaste], open-source code sharing application
    	sam j levy [http://samjlevy.com]

    	This program is free software: you can redistribute it and/or modify it under the
    	terms of the GNU General Public License as published by the Free Software
    	Foundation, either version 3 of the License, or (at your option) any later
    	version.

    	This program is distributed in the hope that it will be useful, but WITHOUT ANY
    	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    	PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    	You should have received a copy of the GNU General Public License along with this
    	program.  If not, see <http://www.gnu.org/licenses/>.
      ====================================================================================*/

	// delete pastes older than 10 (default; see config.php - PRUNE_DAYS) days

	// query old pastes
	$q_prune = "SELECT * FROM " . DB_PREFIX . "pastes WHERE date < DATE_SUB(NOW(), INTERVAL " . PRUNE_DAYS . " DAY)";
	
	// assign query action to a variable, upon failure.. die
	$q_prune_result = mysql_query($q_prune) or die("Querying database failed.");
	
	// count the number of rows found
	$q_prune_count = mysql_num_rows($q_prune_result);
	
	if($q_prune_count != 0) {
		// delete files
		while ($q_prune_row = mysql_fetch_array($q_prune_result)) {
			$id = $q_prune_row["id"];
			
			// remove highlighted file
			if(file_exists(HLIGHT_PATH . $id . ".html")) {
				unlink(HLIGHT_PATH . $id . ".html");
			}
			
			// remove raw file
			if(file_exists(RAW_PATH . $id . ".txt")) {
				unlink(RAW_PATH . $id . ".txt");
			}
		}
		
		// delete entries in database
		$q_prune_pastes = "DELETE FROM " . DB_PREFIX . "pastes WHERE date < DATE_SUB(NOW(), INTERVAL " . PRUNE_DAYS . " DAY)";

		// assign query action to a variable, upon failure.. die
		$q_prune_pastes_result = mysql_query($q_prune_pastes) or die("Querying database failed.");
	}

?>