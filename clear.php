<?php /*====================================================================================
		SamPaste [http://samjlevy.com/sampaste], open-source code sharing application
    	sam j levy [http://samjlevy.com]

    	This program is free software: you can redistribute it and/or modify it under the
    	terms of the GNU General Public License as published by the Free Software
    	Foundation, either version 3 of the License, or (at your option) any later
    	version.

    	This program is distributed in the hope that it will be useful, but WITHOUT ANY
    	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    	PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    	You should have received a copy of the GNU General Public License along with this
    	program.  If not, see <http://www.gnu.org/licenses/>.
      ====================================================================================*/

include("config.php");
include("head.php");

if(isset($_REQUEST['pass']) && $_REQUEST['pass'] == CLEAR_PASSWORD) {

	// query old pastes
	$q_clear = "DELETE FROM " . DB_PREFIX . "pastes";
	
	// assign query action to a variable, upon failure.. die
	$q_clear_result = mysql_query($q_clear) or die("Querying database failed.");

	// delete all files
	$files = glob(HLIGHT_PATH . "*");
	foreach($files as $file) { if($file != ".htaccess") unlink($file); }

	$files = glob(RAW_PATH . "*");
	foreach($files as $file) { if($file != ".htaccess") unlink($file); }

	echo "<br /><br /><center>Clear ran, return to <a href='" . SITE_PATH . "'>index</a></center><br /><br />";
}

include("foot.php");
?>