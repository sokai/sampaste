<?php /*====================================================================================
		SamPaste [http://samjlevy.com/sampaste], open-source code sharing application
    	sam j levy [http://samjlevy.com]

    	This program is free software: you can redistribute it and/or modify it under the
    	terms of the GNU General Public License as published by the Free Software
    	Foundation, either version 3 of the License, or (at your option) any later
    	version.

    	This program is distributed in the hope that it will be useful, but WITHOUT ANY
    	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    	PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    	You should have received a copy of the GNU General Public License along with this
    	program.  If not, see <http://www.gnu.org/licenses/>.
      ====================================================================================*/

include("head.php");
include("prune.php");
?>

<center><br />

<a href="./">index</a></center><br />

<?php

if(isset($_REQUEST["paste"])) {

	// declare error message string
	$error_msg = "";

	// check if paste is blank
	if(trim($_REQUEST["paste"]) == "") {
		$error = 1;
		$error_msg .= "The paste field cannot be left blank.<br />";
	}
	
	// check if paste is greater than max length
	if(strlen($_REQUEST["paste"]) > MAX_LENGTH) {
		$error = 1;
		$error_msg .= "Your paste (" . number_format(strlen($_REQUEST["paste"])) . " characters) is longer than the limit of " . number_format(MAX_LENGTH) . " characters.<br />";
	}

	// check to see if postid already exists (paste has already been made)
	$postid = mysql_real_escape_string(stripslashes($_REQUEST["postid"]));

	$q_postid = "SELECT * FROM " . DB_PREFIX . "pastes WHERE postid = '" . $postid . "'";
	
	// assign query action to a variable, upon failure.. die
	$q_postid_result = mysql_query($q_postid) or die("Querying database failed.");
	
	// count the number of rows found
	$q_postid_count = mysql_num_rows($q_postid_result);
	
	if($q_postid_count != 0) {
		$error = 1;
		$no_goback = 1; // we don't want to offer an option to go back for this error
		$error_msg .= "This paste has already been posted.<br />";
	}

	if($error != 1) {
	
		// retreive this paste's ID
		$id = uniqid();
		
		// grab form values
		$paste = stripslashes($_REQUEST["paste"]);
		$language = trim(mysql_real_escape_string(stripslashes($_REQUEST["language"])));
		$title = trim(mysql_real_escape_string(stripslashes($_REQUEST["title"])));
		$creator = trim(mysql_real_escape_string(stripslashes($_REQUEST["creator"])));
		$visibility = trim(mysql_real_escape_string(stripslashes($_REQUEST["visibility"])));
		
		// establish other values
		$date = date('Y-m-d H:i:s');
		$len = strlen($paste);

		$line = explode("\n", $paste);
		$line = count($line);

		// write entry to database
		$q_add_paste = "INSERT INTO " . DB_PREFIX . "pastes (id, postid, date, language, len, line, visibility";

		// if the title is not left blank
		if($title != ""){
			$q_add_paste .=", title";
		}

		// if the creator is not left blank
		if($creator != ""){
			$q_add_paste .=", creator";
		}

		$q_add_paste .=") VALUES ('$id','$postid','$date','$language',$len,$line,$visibility";

		// if the start date is not blank, include it in the query   
		if($title != ""){
			$q_add_paste .=",'$title'";
		}

		// if the due date is not blank, include it in the query   
		if($creator != ""){
			$q_add_paste .=",'$creator'";
		}

		$q_add_paste .=")";

		// assign query action to a variable, upon failure.. die
		mysql_query($q_add_paste) or die("Database insert failed.");

		// create new file for writing raw
		$file_raw = $id . ".txt";
		$fh_raw = fopen(RAW_PATH . $file_raw, "w") or die("Can't write raw.'");

		// write raw to file
		fwrite($fh_raw, $paste);

		// close file
		fclose($fh_raw);

		// include the GeSHi library
		include_once("geshi/geshi.php");
		
		// create a GeSHi object
		$geshi =& new GeSHi($paste, $language);
	
		$geshi->enable_line_numbers(GESHI_FANCY_LINE_NUMBERS, 5);
		$geshi->set_line_style('background: #fcfcfc;', 'background: #f8f8f8;',true);
	
		// create new file for writing hlight
		$file_hlight = $id . ".html";
		$fh_hlight = fopen(HLIGHT_PATH . $file_hlight, "w") or die("Can't write hlight.'");

		// write hlight to file
		fwrite($fh_hlight, $geshi->parse_code());
	
		// close file
		fclose($fh_hlight);
	
	?>
	
	<center><br /><br /><br />
	<span class="msg_confirm">Paste complete.</span>
	<br /><br />
	
	Your paste: <font style="background-color:#f8f8f8;"><a href="<?php echo SITE_PATH; ?>/v/<?php echo $id; ?>"><?php echo SITE_PATH; ?>/v/<?php echo $id; ?></a></font>
	<br /><br /><br /><br />
    
	<?php } else { ?>
		<center><br /><br /><br /><span class="msg_confirm">Paste failed.</span><br /><br />
		<?php echo $error_msg; ?>
		<?php if(!isset($no_goback)) { ?><br /><a href="javascript:history.go(-1)">Go back</a> and correct the problem<?php } ?>
        <br /><br /><br />
	<?php } } else { ?>

<center>

<script language="javascript">

function commas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

// check length
function checklength(elem){
	var uInput = elem.value;
	
	if(uInput.length == 0) {
		alert("The paste field cannot be left blank.");
		elem.focus();
		return false;
	}
	
	if(uInput.length < <?php echo MAX_LENGTH; ?>){
		return true;
	}
	else{
		alert("Your paste (" + commas(uInput.length) + " characters) is longer than the limit of <?php echo number_format(MAX_LENGTH); ?> characters.");
		elem.focus();
		return false;
	}
}
</script>

<form method="post" action="/p" name="pasted_form" onsubmit="javascript:return checklength(document.getElementById('paste'))">
	Language: <select name="language">
        <option value="4cs">GADV 4CS</option>
        <option value="6502acme">MOS 6502 (6510) ACME Cross Assembler format</option>
        <option value="6502kickass">MOS 6502 (6510) Kick Assembler format</option>
        <option value="6502tasm">MOS 6502 (6510) TASM/64TASS 1.46 Assembler format</option>
    
        <option value="68000devpac">Motorola 68000 - HiSoft Devpac ST 2 Assembler format</option>
        <option value="abap">ABAP</option>
        <option value="actionscript">ActionScript</option>
        <option value="actionscript3">ActionScript 3</option>
        <option value="ada">Ada</option>
        <option value="algol68">ALGOL 68</option>
    
        <option value="apache">Apache configuration</option>
        <option value="applescript">AppleScript</option>
        <option value="apt_sources">Apt sources</option>
        <option value="asm">ASM</option>
        <option value="asp">ASP</option>
        <option value="autoconf">Autoconf</option>
    
        <option value="autohotkey">Autohotkey</option>
        <option value="autoit">AutoIt</option>
        <option value="avisynth">AviSynth</option>
        <option value="awk">awk</option>
        <option value="bascomavr">BASCOM AVR</option>
        <option value="bash">Bash</option>
    
        <option value="basic4gl">Basic4GL</option>
        <option value="bf">Brainfuck</option>
        <option value="bibtex">BibTeX</option>
        <option value="blitzbasic">BlitzBasic</option>
        <option value="bnf">bnf</option>
        <option value="boo">Boo</option>
    
        <option value="c">C</option>
        <option value="c_loadrunner">C (LoadRunner)</option>
        <option value="c_mac">C (Mac)</option>
        <option value="caddcl">CAD DCL</option>
        <option value="cadlisp">CAD Lisp</option>
        <option value="cfdg">CFDG</option>
    
        <option value="cfm">ColdFusion</option>
        <option value="chaiscript">ChaiScript</option>
        <option value="cil">CIL</option>
        <option value="clojure">Clojure</option>
        <option value="cmake">CMake</option>
        <option value="cobol">COBOL</option>
    
        <option value="coffeescript">CoffeeScript</option>
        <option value="cpp">C++</option>
        <option value="cpp-qt">&nbsp;&nbsp;C++ (QT)</option>
        <option value="csharp">C#</option>
        <option value="css">CSS</option>
        <option value="cuesheet">Cuesheet</option>
    
        <option value="d">D</option>
        <option value="dcs">DCS</option>
        <option value="delphi">Delphi</option>
        <option value="diff">Diff</option>
        <option value="div">DIV</option>
        <option value="dos">DOS</option>
    
        <option value="dot">dot</option>
        <option value="e">E</option>
        <option value="ecmascript">ECMAScript</option>
        <option value="eiffel">Eiffel</option>
        <option value="email">eMail (mbox)</option>
        <option value="epc">EPC</option>
    
        <option value="erlang">Erlang</option>
        <option value="f1">Formula One</option>
        <option value="falcon">Falcon</option>
        <option value="fo">FO (abas-ERP)</option>
        <option value="fortran">Fortran</option>
        <option value="freebasic">FreeBasic</option>
    
        <option value="fsharp">F#</option>
        <option value="gambas">GAMBAS</option>
        <option value="gdb">GDB</option>
        <option value="genero">genero</option>
        <option value="genie">Genie</option>
        <option value="gettext">GNU Gettext</option>
    
        <option value="glsl">glSlang</option>
        <option value="gml">GML</option>
        <option value="gnuplot">Gnuplot</option>
        <option value="go">Go</option>
        <option value="groovy">Groovy</option>
        <option value="gwbasic">GwBasic</option>
    
        <option value="haskell">Haskell</option>
        <option value="hicest">HicEst</option>
        <option value="hq9plus">HQ9+</option>
        <option value="html4strict">HTML</option>
        <option value="html5">HTML</option>
        <option value="icon">Icon</option>
    
        <option value="idl">Uno Idl</option>
        <option value="ini">INI</option>
        <option value="inno">Inno</option>
        <option value="intercal">INTERCAL</option>
        <option value="io">Io</option>
        <option value="j">J</option>
    
        <option value="java">Java</option>
        <option value="java5">Java(TM) 2 Platform Standard Edition 5.0</option>
        <option value="javascript">Javascript</option>
        <option value="jquery">jQuery</option>
        <option value="kixtart">KiXtart</option>
        <option value="klonec">KLone C</option>
    
        <option value="klonecpp">KLone C++</option>
        <option value="latex">LaTeX</option>
        <option value="lb">Liberty BASIC</option>
        <option value="lisp">Lisp</option>
        <option value="llvm">LLVM Intermediate Representation</option>
        <option value="locobasic">Locomotive Basic</option>
    
        <option value="logtalk">Logtalk</option>
        <option value="lolcode">LOLcode</option>
        <option value="lotusformulas">Lotus Notes @Formulas</option>
        <option value="lotusscript">LotusScript</option>
        <option value="lscript">LScript</option>
        <option value="lsl2">LSL2</option>
    
        <option value="lua">Lua</option>
        <option value="m68k">Motorola 68000 Assembler</option>
        <option value="magiksf">MagikSF</option>
        <option value="make">GNU make</option>
        <option value="mapbasic">MapBasic</option>
        <option value="matlab">Matlab M</option>
    
        <option value="mirc">mIRC Scripting</option>
        <option value="mmix">MMIX</option>
        <option value="modula2">Modula-2</option>
        <option value="modula3">Modula-3</option>
        <option value="mpasm">Microchip Assembler</option>
        <option value="mxml">MXML</option>
    
        <option value="mysql">MySQL</option>
        <option value="newlisp">newlisp</option>
        <option value="nsis">NSIS</option>
        <option value="oberon2">Oberon-2</option>
        <option value="objc">Objective-C</option>
        <option value="objeck">Objeck Programming Language</option>
    
        <option value="ocaml">OCaml</option>
        <option value="ocaml-brief">&nbsp;&nbsp;OCaml (brief)</option>
        <option value="oobas">OpenOffice.org Basic</option>
        <option value="oracle11">Oracle 11 SQL</option>
        <option value="oracle8">Oracle 8 SQL</option>
        <option value="oxygene">Oxygene (Delphi Prism)</option>
    
        <option value="oz">OZ</option>
        <option value="pascal">Pascal</option>
        <option value="pcre">PCRE</option>
        <option value="per">per</option>
        <option value="perl">Perl</option>
        <option value="perl6">Perl 6</option>
    
        <option value="pf">OpenBSD Packet Filter</option>
        <option value="php" selected="selected">PHP</option>
        <option value="php-brief">&nbsp;&nbsp;PHP (brief)</option>
        <option value="pic16">PIC16</option>
        <option value="pike">Pike</option>
        <option value="pixelbender">Pixel Bender 1.0</option>
    
        <option value="plsql">PL/SQL</option>
        <option value="postgresql">PostgreSQL</option>
        <option value="povray">POVRAY</option>
        <option value="powerbuilder">PowerBuilder</option>
        <option value="powershell">PowerShell</option>
        <option value="proftpd">ProFTPd configuration</option>
    
        <option value="progress">Progress</option>
        <option value="prolog">Prolog</option>
        <option value="properties">PROPERTIES</option>
        <option value="providex">ProvideX</option>
        <option value="purebasic">PureBasic</option>
        <option value="pycon">Python (console mode)</option>
    
        <option value="python">Python</option>
        <option value="q">q/kdb+</option>
        <option value="qbasic">QBasic/QuickBASIC</option>
        <option value="rails">Rails</option>
        <option value="rebol">REBOL</option>
        <option value="reg">Microsoft Registry</option>
    
        <option value="robots">robots.txt</option>
        <option value="rpmspec">RPM Specification File</option>
        <option value="rsplus">R / S+</option>
        <option value="ruby">Ruby</option>
        <option value="sas">SAS</option>
        <option value="scala">Scala</option>
    
        <option value="scheme">Scheme</option>
        <option value="scilab">SciLab</option>
        <option value="sdlbasic">sdlBasic</option>
        <option value="smalltalk">Smalltalk</option>
        <option value="smarty">Smarty</option>
        <option value="sql">SQL</option>
    
        <option value="systemverilog">SystemVerilog</option>
        <option value="tcl">TCL</option>
        <option value="teraterm">Tera Term Macro</option>
        <option value="text">Text</option>
        <option value="thinbasic">thinBasic</option>
        <option value="tsql">T-SQL</option>
    
        <option value="typoscript">TypoScript</option>
        <option value="unicon">Unicon (Unified Extended Dialect of Icon)</option>
        <option value="uscript">Unreal Script</option>
        <option value="vala">Vala</option>
        <option value="vb">Visual Basic</option>
        <option value="vbnet">vb.net</option>
    
        <option value="verilog">Verilog</option>
        <option value="vhdl">VHDL</option>
        <option value="vim">Vim Script</option>
        <option value="visualfoxpro">Visual Fox Pro</option>
        <option value="visualprolog">Visual Prolog</option>
        <option value="whitespace">Whitespace</option>
    
        <option value="whois">Whois (RPSL format)</option>
        <option value="winbatch">Winbatch</option>
        <option value="xbasic">XBasic</option>
        <option value="xml">XML</option>
        <option value="xorg_conf">Xorg configuration</option>
        <option value="xpp">X++</option>
    
        <option value="yaml">YAML</option>
        <option value="z80">ZiLOG Z80 Assembler</option>
        <option value="zxbasic">ZXBasic</option>

	</select>
	<br /><br />
	<textarea name="paste" id="paste" style="width:800px;" rows="20" wrap="off"></textarea><br /><br />
	<table width="400"><tr><td align="right" width="30%">Title:</td><td align="left"><input type="text" name="title" maxlength="30" style="width:100%;"></td></tr>
	<tr><td align="right">Created by:</td><td align="left"><input type="text" name="creator" maxlength="30" style="width:100%;"></td></tr>
    <tr><td align="right">Visibility:</td><td align="left"><select name="visibility"><option value="1">Public</option><option value="0">Private</option></select></td></tr>
    </table><br /><br />
    <center>Entries are automatically removed after <?php echo PRUNE_DAYS; ?> days.</center>
    <br /><br />
    
    <?php	
		// repost protection
		function uuid()
		{
			return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
				mt_rand( 0, 0x0fff ) | 0x4000,
				mt_rand( 0, 0x3fff ) | 0x8000,
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) );
		}
	?>
    
    <input type="hidden" name="postid" value="<?php echo uuid(); ?>">
    
	<input type="submit" name="submit" value="Paste">
</form></center>

<?php } ?>
<br />
<?php include("foot.php"); ?>