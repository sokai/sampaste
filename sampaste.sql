SET FOREIGN_KEY_CHECKS=0;
-- -------------------------------------------
-- Table structure for `dbprefix_pastes
-- If you changed the table prefix (DB_PREFIX;
-- default 'sampaste_') in config.php please
-- change it her in line 8 & 9
-- -------------------------------------------
DROP TABLE IF EXISTS `sampaste_pastes`;
CREATE TABLE `sampaste_pastes` (
  `id` char(13) NOT NULL default '',
  `title` char(30) default NULL,
  `creator` char(30) default NULL,
  `date` datetime default NULL,
  `language` char(30) default NULL,
  `len` int(30) NOT NULL default '0',
  `line` int(30) NOT NULL default '0',
  `visibility` tinyint(1) default NULL,
  `postid` char(36) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;